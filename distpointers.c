#include<stdio.h>
#include<math.h>
void input(float *a1,float *a2,float *b1,float *b2)
{
    printf("Enter the values of a1 & b1\n");
    scanf("%f%f",a1,b1);
    printf("Enter the values of a2 & b2\n");
    scanf("%f%f",a2,b2);
}
float distance(int a1,int a2,int b1,int b2)
{
    float result;
    result=sqrt(pow((a1-a2),2)+pow((b1-b2),2));
    return result;
}
void output(float result)
{
    printf("The distance between 2 points is %f\n",result);
    return;
}
int main()
{
    float a1, a2, b1, b2;
    float result;
    input(&a1,&b1,&a2,&b2);
    result=distance(a1,a2,b1,b2);
    output(result);
    return 0;
} 