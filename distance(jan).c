#include<stdio.h>
#include<math.h>
float distance(float,float,float,float);
float input()
{
    float a1, a2, b1, b2, result;
    printf("Enter the values of a1 & b1\n");
    scanf("%f%f",&a1,&b1);
    printf("Enter the values of a2 & b2\n");
    scanf("%f%f",&a2,&b2);
    result=distance(a1,b1,a2,b2);
    return result;
}
float distance(float a1,float a2,float b1,float b2)
{
    float result;
    result=sqrt(pow((a1-a2),2)+pow((b1-b2),2));
    return result;
}
void output(float result)
{
    printf("The distance between 2 points is %f\n",result);
    return;
}
int main()
{
    float a1, a2, b1, b2;
    float result;
    result=input();
    output(result);
    return 0;
}