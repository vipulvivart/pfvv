
#include <stdio.h>
void swap(int *,int *);
void main()
{
int a,b;
printf("Enter the numbers to be swapped\n");
scanf("%d %d",&a,&b);
printf("The numbers before swapping are \'%d\' and \'%d\'\n",a,b);
swap(&a,&b);
printf("The numbers after swapping are \'%d\' and \'%d\'\n",a,b);
}
void swap(int *a,int *b)
{
 int temp;
 temp=*a;
 *a=*b;
 *b=temp;
}